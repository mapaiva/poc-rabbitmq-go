package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/jexia-com/pocemail/rabbitmq"
	"github.com/sourcegraph/go-ses"
)

func main() {
	if os.Getenv("AWS_ACCESS_KEY_ID") == "" {
		log.Fatal("environment variable 'AWS_ACCESS_KEY_ID' needed but not passed")
	}

	if os.Getenv("AWS_SECRET_KEY") == "" {
		log.Fatal("environment variable 'AWS_SECRET_KEY' needed but not passed")
	}

	if os.Getenv("AWS_SES_ENDPOINT") == "" {
		log.Fatal("environment variable 'AWS_SES_ENDPOINT' needed but not passed")
	}

	from := "matheus@jexia.com"
	to := "matheus.a.paiva@gmail.com"
	res, err := sendEmail(from, to)
	if err == nil {
		fmt.Printf("Sent email: %s...\n", res[:32])
	} else {
		fmt.Printf("Error sending email: %s\n", err)
	}
}

func sendEmail(from, to string) (string, error) {
	subject := "Caramba"
	bodyHTML := `Bateu uma onda <b>FORTE!</b>`
	return ses.EnvConfig.SendEmailHTML(from, to, subject, "", bodyHTML)
}

func exampleConsume() {
	c, err := rabbitmq.NewConsumer(rabbitmq.EmailSignupConfig)
	if err != nil {
		log.Fatal(err)
	}

	msgs, err := c.Consume()
	if err != nil {
		log.Fatal(err)
	}

	forever := make(<-chan bool)
	go func() {
		for d := range msgs {
			// TODO: send emails
			log.Printf("Received a message: %s", d.Body)
			dotCount := bytes.Count(d.Body, []byte("."))
			t := time.Duration(dotCount)
			time.Sleep(t * time.Second)
			log.Printf("Done")
			d.Ack(false)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
